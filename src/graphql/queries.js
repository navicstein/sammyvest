import gql from "graphql-tag";

export const getMe = gql`
  query {
    me {
      id
      username
      email
    }
  }
`;
