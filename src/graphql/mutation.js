import gql from "graphql-tag";

export let loginUser = gql`
  mutation loginUser($identifier: String!, $password: String!) {
    login(input: { identifier: $identifier, password: $password }) {
      jwt
      user {
        id
        username
        provider
      }
    }
  }
`;

export const createUser = gql`
  mutation(
    $username: String!
    $email: String!
    $password: String!
    $fullname: String!
    $phone: String!
  ) {
    register(
      input: {
        username: $username
        email: $email
        fullname: $fullname
        password: $password
        phone: $phone
        isAgent: false
      }
    ) {
      jwt
      user {
        id
        username
        email
      }
    }
  }
`;

export let createBooking = gql`
  mutation($data: BookingInput!) {
    createBooking(input: { data: $data }) {
      booking {
        id
        period
        lodge {
          id
          name
          agent {
            id
            username
            fullname
            phone
          }
        }
        user {
          id
          username
        }
      }
    }
  }
`;
