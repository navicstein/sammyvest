import VueApollo from "vue-apollo";
import ApolloClient from "apollo-boost";
import { InMemoryCache } from "apollo-cache-inmemory";
import { persistCache } from "apollo-cache-persist";
import cookie from "js-cookie";

// TODO: Detect environment and change url
const baseUrl = "http://192.168.43.113:1337";

const token = cookie.get("jwt");
const headers = {};
const cache = new InMemoryCache();

(async () => {
  // await before instantiating ApolloClient ..
  // .. else queries might run before the cache is persisted
  // .. only activate the cache in production
  if (!process.env.DEV) {
    await persistCache({
      cache,
      storage: window.sessionStorage
    });
  }
})();

// If there's a token in cookie store
if (token) {
  // set the bearers' token
  headers["Authorization"] = `Bearer ${token}`;
}

// Create the apollo client
const apolloClient = new ApolloClient({
  cache,
  uri: baseUrl + "/graphql",
  headers,
  resolvers: {}
});

// Create apollo provider
export const apolloProvider = new VueApollo({
  defaultClient: apolloClient,
  //  Custom error handler
  errorHandler({ networkError }) {
    if (networkError && networkError.message.includes("Failed to fetch")) {
      this.$router.push("/no-network");
    }
  }
});

// hit the stream zi
export default ({ app, Vue }) => {
  Vue.use(VueApollo);
  Vue.prototype.baseUrl = baseUrl;
  app.apolloProvider = apolloProvider;
};
