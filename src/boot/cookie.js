import cookie from "js-cookie";
export default async ({ Vue }) => {
  Vue.mixin({
    computed: {
      isAuthenticated() {
        return typeof this.$cookie.get("jwt") !== "undefined";
      }
    }
  });
  window.cookie = cookie;
  Vue.prototype.$cookie = cookie;
};
