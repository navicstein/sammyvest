const routes = [
  {
    path: "/",
    component: () => import("layouts/MyLayout.vue"),
    children: [
      { path: "", component: () => import("pages/Index.vue") },
      { path: "/savings", component: () => import("pages/Savings.vue") },
      { path: "/invest", component: () => import("pages/Invest.vue") },
      {
        path: "/account",
        name: "account",
        component: () => import("pages/Account/Index.vue")
      }
    ]
  },
  {
    path: "/auth",
    component: () => import("layouts/Blank.vue"),
    children: [
      {
        path: "/auth/login",
        name: "login",
        component: () => import("pages/Auth/Login.vue")
      },
      {
        path: "/auth/sign-up",
        name: "signup",
        component: () => import("pages/Auth/SignUp.vue")
      }
    ]
  }
];

// Always leave this as last one
if (process.env.MODE !== "ssr") {
  routes.push({
    path: "*",
    component: () => import("pages/Error404.vue")
  });
}

export default routes;
